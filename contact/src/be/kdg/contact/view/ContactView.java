package be.kdg.contact.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class ContactView extends GridPane {
    private Label lastName;
    private Label firstName;
    private Label email;
    private Label messageBody;
    private TextField lastNameInput;
    private TextField firstNameInput;
    private TextField emailInput;
    private TextArea messageBodyInput;
    private Button submitButton;

    public ContactView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.lastName = new Label("Last name");
        this.firstName = new Label("First name");
        this.email = new Label("Email");
        this.messageBody = new Label("Message");
        this.lastNameInput = new TextField();
        this.firstNameInput = new TextField();
        this.emailInput = new TextField();
        this.messageBodyInput = new TextArea();
        this.submitButton = new Button("Submit");
    }

    private void layoutNodes() {
        this.setPadding(new Insets(10.0, 10.0, 10.0, 10.0));
        this.setHgap(20.0);
        this.setVgap(10.0);

        GridPane.setConstraints(this.lastName, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.lastName, 0, 0);

        GridPane.setConstraints(this.lastNameInput, 1, 0, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER);
        this.add(this.lastNameInput, 1, 0);

        GridPane.setConstraints(this.firstName, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.firstName, 0, 1);

        GridPane.setConstraints(this.firstNameInput, 1, 1, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER);
        this.add(this.firstNameInput, 1, 1);

        GridPane.setConstraints(this.email, 0, 2, 1, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.email, 0, 2);

        GridPane.setConstraints(this.emailInput, 1, 2, 1, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.NEVER);
        this.add(this.emailInput, 1, 2);

        GridPane.setConstraints(this.messageBody, 0, 3, 2, 1, HPos.LEFT, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.messageBody, 0, 3);

        GridPane.setConstraints(this.messageBodyInput, 0, 4, 2, 1, HPos.LEFT, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(this.messageBodyInput, 0, 4);

        GridPane.setConstraints(this.submitButton, 0, 5, 2, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.submitButton, 0, 5);
    }

    Button getSubmitButton() {
        return this.submitButton;
    }

    TextField getLastNameInput() {
        return lastNameInput;
    }

    TextField getFirstNameInput() {
        return firstNameInput;
    }

    TextField getEmailInput() {
        return emailInput;
    }

    TextArea getMessageBodyInput() {
        return messageBodyInput;
    }

    void clearInput() {
        getLastNameInput().clear();
        getFirstNameInput().clear();
        getEmailInput().clear();
        getMessageBodyInput().clear();
        getLastNameInput().requestFocus();
    }
}
