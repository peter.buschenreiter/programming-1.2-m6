package be.kdg.contact.view;

import be.kdg.contact.model.Message;
import be.kdg.contact.model.Messenger;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.IOException;

public class ContactPresenter {
    private final Messenger model;
    private final ContactView view;

    public ContactPresenter(Messenger model, ContactView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getSubmitButton().setOnAction(event -> {
            final String lastName = view.getLastNameInput().getText();
            final String firstName = view.getFirstNameInput().getText();
            final String email = view.getEmailInput().getText();
            final String messageBody = view.getMessageBodyInput().getText();

            Message message = new Message(lastName, firstName, email, messageBody);
            try {
                model.send(message);
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "Message sent",ButtonType.OK);
                alert.setHeaderText("Message sent");
                alert.show();
            } catch (IOException e) {
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR, "Message failed", ButtonType.CLOSE);
                alert.setHeaderText("Error sending message");
                alert.show();
            }
            updateView();
        });
    }

    private void updateView() {
        view.clearInput();
    }
}
