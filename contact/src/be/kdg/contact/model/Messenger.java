package be.kdg.contact.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Messenger {
	public void send(Message message) throws IOException {
		save(message);
	}

	private void save(Message message) throws IOException {
		String fileName = String.format("contact/src/be/kdg/contact/%s_%s.txt", message.getLastName(), message.getFirstName());
		try (PrintWriter printWriter =
				new PrintWriter(
						new BufferedWriter(
								new FileWriter(fileName)))) {

			printWriter.format("""
				Last name:\t %s
				First name:\t %s
				Email:\t %s
				Message body:
				%s
				""", message.getLastName(), message.getFirstName(), message.getEmail(), message.getMessageBody());
		} catch (IOException e) {
			throw new IOException(e);
		}
	}
}
