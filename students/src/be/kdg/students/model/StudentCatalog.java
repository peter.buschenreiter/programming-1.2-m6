package be.kdg.students.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StudentCatalog {
	private final List<Student> students;
	private final InputStream inputStream;

	public StudentCatalog() {
		students = new ArrayList<>();
		inputStream = getClass().getClassLoader().getResourceAsStream("students.txt");
		readStudents();
	}

	public List<Student> getStudents() {
		return this.students;
	}

	private void readStudents() {
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
			bufferedReader.readLine();
			String line = bufferedReader.readLine();
			while (line != null) {
				String[] inputs = line.split(";");
				Student student = new Student(inputs[0], inputs[1], Integer.parseInt(inputs[2]), inputs[3]);
				students.add(student);
				line = bufferedReader.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
