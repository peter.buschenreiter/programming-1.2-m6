package be.kdg.students.view;

import be.kdg.students.model.Student;
import be.kdg.students.model.StudentCatalog;

import java.util.List;

public class StudentPresenter {
    private final StudentCatalog model;
    private final StudentView view;

    private int currentStudent;

    public StudentPresenter(StudentCatalog model, StudentView view) {
        this.model = model;
        this.view = view;
        currentStudent = 0;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getPrevious().setOnAction(event -> {
            currentStudent--;
            updateView();
        });

        view.getNext().setOnAction(event -> {
            currentStudent++;
            updateView();
        });
    }

    private void updateView() {
        if (currentStudent == 0) {
            view.getPrevious().setDisable(true);
        } else {
            view.getPrevious().setDisable(false);
        }

        if (currentStudent >= this.model.getStudents().size() - 1) {
            view.getNext().setDisable(true);
        } else {
            view.getNext().setDisable(false);
        }

        final List<Student> students = model.getStudents();
        if (!students.isEmpty()) {
            final Student student = students.get(currentStudent);
            view.getFirstNameInput().setText(student.getLastName());
            view.getLastNameInput().setText(student.getFirstName());
            view.getNumberInput().setText(String.valueOf(student.getNumber()));
            view.getGroupInput().setText(student.getGroup());
        }
    }
}
