package be.kdg.message;

import be.kdg.message.view.MessagePresenter;
import be.kdg.message.view.MessageView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MessageApplication extends Application {
    @Override
    public void start(Stage primaryStage) {
        final MessageView view = new MessageView();
        new MessagePresenter(view);
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Message");
        // primaryStage.setResizable(false); // bugged under Linux
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
