package be.kdg.message.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.Base64;

public class MessagePresenter {
	private final MessageView view;

	public MessagePresenter(MessageView view) {
		this.view = view;

		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		final EventHandler<ActionEvent> eventHandler = event -> updateView();
		view.getForegroundPicker().setOnAction(eventHandler);
		view.getBackgroundPicker().setOnAction(eventHandler);

		view.getMessageField().textProperty().addListener((observable, oldValue, newValue) -> updateView());

		view.getLoadMenuItem().setOnAction(event -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("bin files", "*.bin"));
			fileChooser.setTitle("Open");
			File file = fileChooser.showOpenDialog(view.getScene().getWindow());
			if (file != null) {
				try (DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file))) {
					final short redF = (dataInputStream.readShort());
					final short greenF = (dataInputStream.readShort());
					final short blueF = (dataInputStream.readShort());
					final short redB = (dataInputStream.readShort());
					final short greenB = (dataInputStream.readShort());
					final short blueB = (dataInputStream.readShort());
					byte[] buffer = new byte[50];
					int byteAmount = dataInputStream.read(buffer);
					String encodedMessage = new String(buffer, 0, byteAmount);
					String message = new String(Base64.getDecoder().decode(encodedMessage));

					view.getMessageField().setText(message);
					view.getForegroundPicker().setValue(Color.rgb(redF, greenF, blueF));
					view.getBackgroundPicker().setValue(Color.rgb(redB, greenB, blueB));
					updateView();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		view.getSaveMenuItem().setOnAction(event -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("bin files", "*.bin"));
			fileChooser.setTitle("Save");
			File file = fileChooser.showSaveDialog(view.getScene().getWindow());
			if (file != null) {
				try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file))) {
					final String message = view.getMessageField().getText();
					final Color foreground = view.getForegroundPicker().getValue();
					final Color background = view.getBackgroundPicker().getValue();
					final short redF = (short) (foreground.getRed() * 255);
					final short greenF = (short) (foreground.getGreen() * 255);
					final short blueF = (short) (foreground.getBlue() * 255);
					final short redB = (short) (background.getRed() * 255);
					final short greenB = (short) (background.getGreen() * 255);
					final short blueB = (short) (background.getBlue() * 255);
					dataOutputStream.writeShort(redF);
					dataOutputStream.writeShort(greenF);
					dataOutputStream.writeShort(blueF);
					dataOutputStream.writeShort(redB);
					dataOutputStream.writeShort(greenB);
					dataOutputStream.writeShort(blueB);
					dataOutputStream.write(Base64.getEncoder().encode(message.getBytes()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void updateView() {
		final String message = view.getMessageField().getText();
		final Color foreground = view.getForegroundPicker().getValue();
		final Color background = view.getBackgroundPicker().getValue();
		view.showMessage(message, foreground, background);
	}
}
