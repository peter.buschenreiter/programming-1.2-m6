package be.kdg.files;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 1. Create a new directory called module6. It is to be created in the current working directory of your Java application.<p></p>
 Hint: it's best to check if it exists before creating it. This way you can easily execute your program multiple times.<p></p>
 2. Create a new file called my_file.txt in this directory. Again, check if it exists first.<p></p>
 3. Write your first name and last name to this file. Check the documentation of Files (Links to an external site.). Using this class, you can pull this off using a single method call.<p></p>
 4. Obtain your file's attributes using the readAttributes method of Files. We'd like to retrieve all attributes. Check the documentation (Links to an external site.) again to see which arguments you need to pass.<p></p>
 5. Print all the file's attributes and their values using a formatted string.<p></p>
 6. Finally, create a copy of your file called my_files_copy.txt.<p></p>
 7. Open both files using a basic text editor such as notepad, and check their contents.<p></p>
 */

public class Main {
	public static void main(String[] args) {
		Path directory = Paths.get("files/src/be/kdg/files/module6");
		Path file = Paths.get("files/src/be/kdg/files/module6/my_file.txt");
		String firstName = "Peter";
		String lastName = "Buschenreiter";
		Path fileCopy = Paths.get("files/src/be/kdg/files/module6/my_files_copy.txt");

		if (Files.notExists(directory)) {
			try {
				Files.createDirectory(directory);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (Files.notExists(file)) {
			try {
				Files.createFile(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			Files.write(file, (firstName + " " + lastName).getBytes());
			Files.readAttributes(file, "*").forEach((s, o) -> System.out.printf("%25s: %s\n", s, o));
			Files.copy(file, fileCopy);
		} catch (FileAlreadyExistsException e) {
			System.out.println("File already exists. Not recreated.");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
