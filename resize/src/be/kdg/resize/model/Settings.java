package be.kdg.resize.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

public class Settings {
	private static final File SETTINGS_FILE = new File("resize/src/be/kdg/resize/settings.txt");

	public static void saveSettings(Map<String, Double> settings) {
		try (Formatter formatter = new Formatter(SETTINGS_FILE)) {
			settings.forEach((settingName, setting) -> formatter.format("%s=%s\n", settingName, setting));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Map<String, Double> loadSettings() {
		Map<String, Double> newSettings = new HashMap<>();
		try (FileReader fileReader = new FileReader(SETTINGS_FILE)) {
			char[] buffer = new char[100];
			int amountOfChars = fileReader.read(buffer);
			String bufferText = new String(buffer, 0, amountOfChars);
			String[] lines = bufferText.split("\n");
			for (String line : lines) {
				String[] options = line.split("=");
				String settingName = options[0];
				Double setting = Double.valueOf(options[1]);
				newSettings.put(settingName, setting);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return newSettings;
	}
}
