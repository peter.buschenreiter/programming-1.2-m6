package be.kdg.mailinglist.model;

import java.io.*;
import java.util.*;

public class MailingList {
	private static final File FILE = new File("mailinglist/src/be/kdg/mailinglist/email_addresses.txt");

	private final Set<EmailAddress> addresses;

	public MailingList() {
		this.addresses = new TreeSet<>();
	}

	public void addEmailAddress(EmailAddress address) {
		this.addresses.add(address);
	}

	public List<String> getEmailAddresses() {
		final List<String> stringList = this.addresses.stream()
		                                              .map(EmailAddress::toString).toList();
		return Collections.unmodifiableList(stringList);
	}

	public void removeEmailAddress(String address) {
		for (Iterator<EmailAddress> i = this.addresses.iterator(); i.hasNext(); ) {
			EmailAddress emailAddress = i.next();
			if (emailAddress.toString().equals(address)) {
				i.remove();
				return;
			}
		}
	}

	public void loadFromDisk() throws IOException {
		try (Scanner scanner = new Scanner(FILE)) {
			while (scanner.hasNext()) {
				addresses.add(new EmailAddress(scanner.nextLine()));
			}
		}
	}

	public void saveToDisk() throws IOException {
		try (PrintWriter printWriter =
				     new PrintWriter(
						     new BufferedWriter(
								     new FileWriter(FILE)))) {
			for (EmailAddress address : addresses) {
				printWriter.format("%s\n", address);
			}
		}
	}
}
