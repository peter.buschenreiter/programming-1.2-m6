package be.kdg.mailinglist.view;

import be.kdg.mailinglist.model.EmailAddress;
import be.kdg.mailinglist.model.EmailAddressVerification;
import be.kdg.mailinglist.model.MailingList;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

import java.io.IOException;
import java.util.List;

public class Presenter {
    private final MailingList model;
    private final MailingListView view;

    private boolean isCurrentEmailAddressValid;

    public Presenter(MailingList model, MailingListView view) {
        this.model = model;
        this.view = view;

        this.isCurrentEmailAddressValid = false;

        try {
            this.model.loadFromDisk();
        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error while loading email addresses.");
            alert.setContentText(ex.getMessage());
            alert.setResizable(true); // Due to a bug under Linux
            alert.showAndWait();
        }

        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers() {
        this.view.getEmailTextField().textProperty().addListener((observable, oldValue, newValue) -> {
            this.isCurrentEmailAddressValid = EmailAddressVerification.isValidEmailAddress(newValue);
            updateView();
        });

        final EventHandler<ActionEvent> eventHandler = event -> {
            if (!view.getAdd().isDisabled()) {
                model.addEmailAddress(new EmailAddress(view.getEmailTextField().getText()));
                this.view.getEmailTextField().clear();
                this.isCurrentEmailAddressValid = false;
                updateView();
            }
        };

        this.view.getEmailTextField().setOnAction(eventHandler);
        this.view.getAdd().setOnAction(eventHandler);

        this.view.getDelete().setOnAction(event -> {
            if (this.view.getList().getSelectionModel().getSelectedIndex() != -1) {
                model.removeEmailAddress(view.getList().getSelectionModel().getSelectedItem());
                updateView();
            }
        });

        this.view.getSave().setOnAction(event -> {
            try {
                model.saveToDisk();
            } catch (IOException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error while saving email addresses.");
                alert.setContentText(ex.getMessage());
                alert.setResizable(true); // Due to a bug under Linux
                alert.showAndWait();
            }
        });
    }

    private void updateView() {
        final List<String> addresses = this.model.getEmailAddresses();
        this.view.getList().setItems(FXCollections.observableArrayList(addresses));
        this.view.getDelete().setDisable(addresses.size() == 0);
        this.view.getAdd().setDisable(!this.isCurrentEmailAddressValid);
    }
}
