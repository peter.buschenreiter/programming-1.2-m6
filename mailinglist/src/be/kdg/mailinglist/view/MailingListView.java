package be.kdg.mailinglist.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class MailingListView extends GridPane {
    private Label emailLabel;
    private TextField emailTextField;
    private Button add;
    private ListView<String> list;
    private Button delete;
    private Button save;

    public MailingListView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.emailLabel = new Label("Email address");
        this.emailTextField = new TextField();
        this.add = new Button("Add >>");
        this.list = new ListView<>();
        this.delete = new Button("Delete");
        this.save = new Button("Save");

        this.list.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.add.setDisable(true);
    }

    private void layoutNodes() {
        this.setPadding(new Insets(15.0));
        this.setHgap(15.0);
        this.setVgap(15.0);

        GridPane.setConstraints(this.emailLabel, 0, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.emailLabel, 0, 0);
        GridPane.setConstraints(this.emailTextField, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.emailTextField, 1, 0);
        GridPane.setConstraints(this.add, 0, 1, 2, 2, HPos.CENTER, VPos.TOP, Priority.NEVER, Priority.ALWAYS);
        this.add(this.add, 0, 1);

        GridPane.setConstraints(this.list, 2, 0, 1, 2, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(this.list, 2, 0);
        this.delete.setMaxWidth(Double.MAX_VALUE);
        GridPane.setConstraints(this.delete, 2, 2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER);
        this.add(this.delete, 2, 2);

        this.save.setPrefWidth(200.0);
        GridPane.setConstraints(this.save, 0, 3, 3, 1, HPos.CENTER, VPos.CENTER, Priority.NEVER, Priority.NEVER);
        this.add(this.save, 0, 3);
    }

    Button getAdd() {
        return this.add;
    }

    TextField getEmailTextField() {
        return this.emailTextField;
    }

    ListView<String> getList() {
        return this.list;
    }

    Button getDelete() {
        return this.delete;
    }

    Button getSave() {
        return this.save;
    }
}
